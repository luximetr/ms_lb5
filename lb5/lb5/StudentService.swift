//
//  StudentService.swift
//  lb5
//
//  Created by Aleksandr on 10.03.17.
//  Copyright © 2017 Alexandr Orlov. All rights reserved.
//

import Foundation

class StudentService: NSObject {
    var marks: Array<Int> = []
    
    init(marks: Array<Int>) {
        super.init()
        for i in 0..<marks.count {
            if self.isValidMark(marks[i]) {
                self.marks.append(marks[i])
            }
        }
    }
    
    func addMark(_ mark: Int) {
        if self.isValidMark(mark) {
            self.marks.append(mark)
        }
    }
    
    func isValidMark(_ mark: Int) -> Bool {
        return mark >= 1 && mark <= 5
    }
}
