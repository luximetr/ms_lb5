//
//  lb5Tests.swift
//  lb5Tests
//
//  Created by Aleksandr on 10.03.17.
//  Copyright © 2017 Alexandr Orlov. All rights reserved.
//

import XCTest
@testable import lb5

class lb5Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test001_initWithMarks() {
        let studentService = StudentService(marks: [5, 4])
        XCTAssertTrue(studentService.marks.count == 2, "Обьект проинициализирован неправильно")
    }
    
    func test002_isInitWithValidMarks() {
        let studentService = StudentService(marks: [4, 5, 6])
        XCTAssertTrue(studentService.marks.count == 2, "6 - оценка не может быть добавлена")
    }
    
    func test003_isValidZeroMark() {
        let studentService = StudentService(marks: [])
        XCTAssertFalse(studentService.isValidMark(0), "Оценка не может быть меньше 1")
    }
    
    func test004_isValidMoreThanFiveMark() {
        let studentService = StudentService(marks: [])
        XCTAssertFalse(studentService.isValidMark(7), "Оценка не может быть больше 5")
    }
    
    func test005_isValidMarkFive() {
        let studentService = StudentService(marks: [])
        XCTAssertTrue(studentService.isValidMark(5))
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
